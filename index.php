<html>
<head><title> Connect 4 - Play Game </title></head>
<body>
<table width="600" height="400" border="30" bordercolor="blue">

    <?php
	
	session_start();

    $whiteflag='img/white.jpg';
    $redflag='img/red.jpg';
    $yellowflag='img/yellow.jpg';
	$red = 'RED';
	$yellow = 'YELLOW';
	$castigator = false;
	
// verifica daca exista tabela in sesiune, daca nu, o creaza 
if(isset($_SESSION['table'])) { $table = $_SESSION['table']; } 
else {
$_SESSION['table'] = [
    [false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false],
]; 
$player = $red;} //la primul joc, incepe rosul;
	
	$table = $_SESSION['table']; 
	
	if(isset($_GET['nextplayer']))  {
	 $player = $_GET['nextplayer'];
	 } else {$player = $red;}
	 
$nextmove = false;
 if(isset($_GET['nextmove']))  {
	 $nextmove = $_GET['nextmove'];
 }
 
if(isset($_SESSION['player'])) {
$player = $_SESSION['player'];
}

if ($player == $red) { 
$nextplayer = $yellow;
	} else {$nextplayer = $red;
			}

for ($i=5; $i>=0; $i--) {
		if ($table[$i][$nextmove] === false) { 
		$table[$i][$nextmove] = $player; break;
		//echo "$i $nextmove - Eureka";
		 
		 }
	}
	
//	echo "<font color='$player'><center><h2>Jucatorul $player a selectat coloana $nextmove</h2>";


    for ($i=0; $i<6; $i++) {
        echo "<tr>";
        for ($j=0; $j<7; $j++) { 
            if ($table[$i][$j] === false) {
                echo "<td><a href='index.php?nextplayer=$nextplayer&nextmove=$j'><img src='$whiteflag'></a></td>";
        }
       elseif ($table[$i][$j] === $red) {
          echo "<td><img src='$redflag'></td>";
       } else { echo "<td><img src='$yellowflag'></td>";  }
		}
        echo "</tr>";
    
	}
//	echo "<font color='$nextplayer'><center><h1>Culoarea $nextplayer muta</h1></font>";
    $_SESSION['table'] = $table;
	$_SESSION['player'] = $nextplayer;
	
    //validare pe linii
    for ($i = 0; $i < 6; $i++) {
        if ($table[$i][0] == $table[$i][1] && $table[$i][1] == $table[$i][2]&& $table[$i][2] == $table[$i][3] && $table[$i][0] != false) {
            $castigator = $table[$i][0]; echo "<center><h1>Castigator pe linia ".($i+1)."</h1>";
        }
		if ($table[$i][1] == $table[$i][2] && $table[$i][2] == $table[$i][3]&& $table[$i][3] == $table[$i][4] && $table[$i][4] != false) {
            $castigator = $table[$i][1]; echo "<center><h1>Castigator pe linia ".($i+1)."</h1>";
        }
		if ($table[$i][2] == $table[$i][3] && $table[$i][3] == $table[$i][4]&& $table[$i][4] == $table[$i][5] && $table[$i][5] != false) {
            $castigator = $table[$i][2]; echo "<center><h1>Castigator pe linia ".($i+1)."</h1>";
        }
		if ($table[$i][3] == $table[$i][4] && $table[$i][4] == $table[$i][5]&& $table[$i][5] == $table[$i][6] && $table[$i][6] != false) {
            $castigator = $table[$i][3]; echo "<center><h1>Castigator pe linia ".($i+1)."</h1>";
        }
    }
    //validare pe coloane
    for ($i = 0; $i < 7; $i++) {
        if ($table[0][$i] == $table[1][$i] && $table[1][$i] == $table[2][$i] && $table[2][$i] == $table[3][$i] && $table[0][$i] != false) {
            $castigator = $table[0][$i]; echo "<center><h1>Castigator pe coloana ".($i+1)."</h1>";
        }
		if ($table[1][$i] == $table[2][$i] && $table[2][$i] == $table[3][$i] && $table[3][$i] == $table[4][$i] && $table[4][$i] != false) {
            $castigator = $table[1][$i]; echo "<center><h1>Castigator pe coloana ".($i+1)."</h1>";
        }
		if ($table[2][$i] == $table[3][$i] && $table[3][$i] == $table[4][$i] && $table[4][$i] == $table[5][$i] && $table[5][$i] != false) {
            $castigator = $table[2][$i]; echo "<center><h1>Castigator pe coloana ".($i+1)."</h1>";
        }
	}

    //validare pe diagonale principala stanga-dreapta
    if ($table[0][0] == $table[1][1] && $table[1][1] == $table[2][2]&& $table[2][2] == $table[3][3] && $table[0][0] != false) {
        $castigator = $table[0][0]; echo "<center><h1>Castigator pe diagonala </h1>";
    }
	if ($table[1][1] == $table[2][2] && $table[2][2] == $table[3][3]&& $table[3][3] == $table[4][4] && $table[1][1] != false) {
        $castigator = $table[1][1]; echo "<center><h1>Castigator pe diagonala </h1>";
    }
	if ($table[2][2] == $table[3][3] && $table[3][3] == $table[4][4]&& $table[4][4] == $table[5][5] && $table[2][2] != false) {
        $castigator = $table[2][2]; echo "<center><h1>Castigator pe diagonala </h1>";
    }
    if ($table[1][0] == $table[2][1] && $table[2][1] == $table[3][2]&& $table[3][2] == $table[4][3] && $table[4][3] != false) {
        $castigator = $table[3][2]; echo "<center><h1>Castigator pe diagonala </h1>";
    }
	 if ($table[2][1] == $table[3][2] && $table[3][2] == $table[4][3]&& $table[4][3] == $table[5][4] && $table[5][4] != false) {
        $castigator = $table[5][4]; echo "<center><h1>Castigator pe diagonala </h1>";
    }
	 if ($table[2][0] == $table[3][1] && $table[3][1] == $table[4][2]&& $table[4][2] == $table[5][3] && $table[5][3] != false) {
        $castigator = $table[4][2]; echo "<center><h1>Castigator pe diagonala </h1>";
    }
	 if ($table[0][1] == $table[1][2] && $table[1][2] == $table[2][3]&& $table[2][3] == $table[3][4] && $table[3][4] != false) {
        $castigator = $table[2][3]; echo "<center><h1>Castigator pe diagonala </h1>";
    }
	if ($table[1][2] == $table[2][3] && $table[2][3] == $table[3][4]&& $table[3][4] == $table[4][5] && $table[4][5] != false) {
        $castigator = $table[4][5]; echo "<center><h1>Castigator pe diagonala </h1>";
    }
	if ($table[2][3] == $table[3][4] && $table[3][4] == $table[4][5]&& $table[4][5] == $table[5][6] && $table[4][5] != false) {
        $castigator = $table[5][6]; echo "<center><h1>Castigator pe diagonala </h1>";
    }
	if ($table[0][2] == $table[1][3] && $table[1][3] == $table[2][4]&& $table[2][4] == $table[3][5] && $table[3][5] != false) {
        $castigator = $table[3][6]; echo "<center><h1>Castigator pe diagonala </h1>";
    }
	if ($table[1][3] == $table[2][4] && $table[2][4] == $table[3][5]&& $table[3][5] == $table[4][6] && $table[4][6] != false) {
        $castigator = $table[4][6]; echo "<center><h1>Castigator pe diagonala </h1>";
    }
	if ($table[0][3] == $table[1][4] && $table[1][4] == $table[2][5]&& $table[2][5] == $table[3][6] && $table[3][6] != false) {
        $castigator = $table[3][6]; echo "<center><h1>Castigator pe diagonala </h1>";
    }
	
	
    if ($castigator != false) {
        echo "\n <center><h2>Castigatorul este $castigator</h2></center><br />";
        echo "<form id='form1' name='form1' method='post' action='index.php'>
  <label>
  <input type='submit' name='Submit' value='Joc Nou' />
  </label>
</form>";
        session_unset();
    }
	
	//parcurge matricea sa verifice daca mai exista casuta goala
    $casuta_goala = false;
    foreach ($table as $tabel) {
        foreach ($tabel as $item)
        {
            if ($item === false) {
                $casuta_goala = true;
            }

        }

    }
    if (($castigator == false) && ($casuta_goala==false)) {
        echo "nu a fost gasit niciun castigator";
        echo "<form id='form2' name='form2' method='post' action='index.php'>
  <label>
  <input type='submit' name='Submit' value='Joc Nou' />
  </label>
</form>";
        session_unset(); }

    ?>
</table>
</body>
</html>